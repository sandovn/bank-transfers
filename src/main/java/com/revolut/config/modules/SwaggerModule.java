package com.revolut.config.modules;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

import javax.annotation.PostConstruct;

/**
 * A google guice module for configuring swagger
 * For example: hostname and schemes
 */
public class SwaggerModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ApiListingResource.class).asEagerSingleton();
        bind(SwaggerSerializers.class).asEagerSingleton();
    }

    @PostConstruct
    public void initializeSwagger() {
        BeanConfig swaggerBeanConfig = new BeanConfig();
        swaggerBeanConfig.setHost("localhost:8080");
        swaggerBeanConfig.setSchemes(new String[]{"http"});
        swaggerBeanConfig.setResourcePackage("com.revolut.controllers");
        swaggerBeanConfig.setDescription("This is a simple transaction service allowing you to do payments from one bank account to another");
        swaggerBeanConfig.setScan(true);
        swaggerBeanConfig.setBasePath("/api/v1");

        bind(new TypeLiteral<BeanConfig>() {}).toInstance(swaggerBeanConfig);
    }

}
