package com.revolut.config.modules;

import com.google.inject.AbstractModule;
import com.revolut.service.AccountService;
import com.revolut.service.BankAccountService;
import com.revolut.service.PaymentTransactionService;
import com.revolut.service.TransactionService;

public class DataServicesModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(TransactionService.class).to(PaymentTransactionService.class);
        bind(AccountService.class).to(BankAccountService.class);
    }
}
