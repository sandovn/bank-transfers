package com.revolut.config.modules;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.revolut.model.Account;
import com.revolut.model.Transaction;
import com.revolut.service.data.DataSource;
import com.revolut.service.data.InMemoryDataSource;

public class DataSourceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(new TypeLiteral<DataSource<Transaction>>() {}).toInstance(new InMemoryDataSource<Transaction>());
        bind(new TypeLiteral<DataSource<Account>>() {}).toInstance(new InMemoryDataSource<Account>());
    }
}
