package com.revolut.config;

import com.google.inject.servlet.ServletModule;
import com.revolut.config.modules.DataServicesModule;
import com.revolut.config.modules.DataSourceModule;
import com.revolut.config.modules.SwaggerModule;

public class BindServicesModule extends ServletModule {
    @Override
    protected void configureServlets() {
        super.configureServlets();
        install(new DataSourceModule());
        install(new DataServicesModule());
        install(new SwaggerModule());
    }
}
