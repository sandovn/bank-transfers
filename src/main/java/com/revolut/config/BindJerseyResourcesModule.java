package com.revolut.config;

import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import org.glassfish.jersey.server.ResourceConfig;

import java.util.Set;

public class BindJerseyResourcesModule extends JerseyServletModule {

    @Override
    protected void configureServlets() {
        bind(ApiListingResource.class).asEagerSingleton();
        bind(SwaggerSerializers.class).asEagerSingleton();

        bindResources();
        serveBoundResources();
    }

    private void bindResources() {
        for (Class<?> resource : lookupResources()) {
            bind(resource);
        }
    }

    private Set<Class<?>> lookupResources() {
        ResourceConfig rc = new ResourceConfig().packages(
                "com.revolut.controllers",
                "io.swagger.jaxrs2.integration.resources"
        );
        return rc.getClasses();
    }

    private void serveBoundResources() {
        serve("/api/v1/*").with(GuiceContainer.class);
    }
}
