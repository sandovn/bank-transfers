package com.revolut.controllers;

import com.google.inject.Inject;
import com.google.inject.servlet.RequestScoped;
import com.revolut.model.Account;
import com.revolut.model.Transaction;
import com.revolut.model.transaction.TransactionState;
import com.revolut.service.AccountService;
import com.revolut.service.TransactionService;
import com.revolut.service.exception.DuplicateElementException;
import com.revolut.service.exception.ElementNotFoundException;
import com.revolut.service.exception.InvalidTransactionAmountException;
import io.swagger.annotations.Api;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Api(value = "data-seed", basePath = "/api/v1")
@Path("/data")
@RequestScoped
@Produces(MediaType.APPLICATION_JSON)
public class DataSeedController {

    @Inject
    private TransactionService transactionService;

    @Inject
    private AccountService accountService;

    @POST
    public String dataSeed() {
        Account account1 = null, account2 = null, account3 = null, account4 = null;
        try {
            account1 = new Account("John Doe", "087659642", "23-01-03", true);
            account1.setBalance(BigDecimal.valueOf(1000));
            account2 = new Account("Jane Nichols", "56789873", "23-45-03", true);
            account2.setBalance(BigDecimal.valueOf(1000));
            account3 = new Account("Patrick Stuart", "03967302", "23-77-66", true);
            account3.setBalance(BigDecimal.valueOf(1000));
            account4 = new Account("Bob Dilan", "2839403725", "23-90-53", true);
            account4.setBalance(BigDecimal.valueOf(1000));

            accountService.store(account1);
            accountService.store(account2);
            accountService.store(account3);
            accountService.store(account4);
        } catch (DuplicateElementException dee) {
            dee.printStackTrace();
        }

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1, date2, date3, date4, date5;
            date1 = formatter.parse("2018-02-02 12:34:01");
            date2 = formatter.parse("2015-10-22 18:28:48");
            date3 = formatter.parse("2017-07-12 10:04:08");
            date4 = formatter.parse("2016-07-19 04:27:17");
            date5 = formatter.parse("2011-07-19 15:54:29");

            transactionService.store(new Transaction(TransactionState.FULFILLED, account1, account2, BigDecimal.valueOf(23.6), date1));
            transactionService.store(new Transaction(TransactionState.PENDING, account1, account3, BigDecimal.valueOf(273.0), date2));
            transactionService.store(new Transaction(TransactionState.PROCESSING, account4, account1, BigDecimal.valueOf(523.06), date3));
            transactionService.store(new Transaction(TransactionState.CHARGEBACK, account3, account2, BigDecimal.valueOf(3.6399), date4));
            transactionService.store(new Transaction(TransactionState.CANCELLED, account1, account4, BigDecimal.valueOf(780.65), date5));
        } catch (DuplicateElementException | ParseException | InvalidTransactionAmountException | ElementNotFoundException e) {
            e.printStackTrace();
        }

        return "Success";
    }
}
