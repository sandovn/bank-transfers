package com.revolut.controllers;

import com.google.inject.Inject;
import com.google.inject.servlet.RequestScoped;
import com.revolut.model.Transaction;
import com.revolut.model.transaction.TransactionState;
import com.revolut.service.TransactionService;
import com.revolut.service.exception.DuplicateElementException;
import com.revolut.service.exception.ElementNotFoundException;
import com.revolut.service.exception.InvalidTransactionAmountException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * Base controller to handle all transaction operations
 */
@Api(value = "transaction-service", basePath = "/api/v1")
@Path("/transactions")
@RequestScoped
@Produces(MediaType.APPLICATION_JSON)
public class TransactionController {

    private TransactionService transactionService;

    @Inject
    TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    /**
     * Generates a list of all transactions
     * @return List of ransactions
     */
    @GET
    @ApiOperation(value = "Fetches all available transactions",
            notes = "All transactions",
            response = Transaction.class,
            responseContainer = "List")
    public Response getTransactions() {
        return Response.ok(transactionService.getAll()).build();
    }

    /**
     * Get a transaction based on its ID. This will generate 404
     * if the transaction does not exists.
     * @return List of ransactions
     */
    @GET
    @Path("/{transactionId}")
    public Response getTransaction(@PathParam("transactionId") UUID transactionId) {
        try {
            return Response.ok(transactionService.get(transactionId)).build();
        } catch (ElementNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).entity("Transaction was not found").build();
        }
    }

    /**
     * Transfer money from one account to another
     * @param fromAccountId get money from
     * @param toAccountId put money in
     * @param amount money to be transfered
     * @return the actual result
     */
    @POST
    @Path("/{fromAccountId}/{toAccountId}/{amount}")
    public Response createTransaction(
            @PathParam("fromAccountId") UUID fromAccountId,
            @PathParam("toAccountId") UUID toAccountId,
            @PathParam("amount") BigDecimal amount
    ) {

        Transaction transaction;
        try {
            transaction = transactionService.store(fromAccountId, toAccountId, amount);
        } catch (DuplicateElementException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Internal Server Error")
                    .build();
        } catch (ElementNotFoundException | InvalidTransactionAmountException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(e.getMessage())
                    .build();
        }

        return Response.status(Response.Status.CREATED).entity(transaction).build();
    }

    @PATCH
    @Path("/{transactionId}/{state}")
    @ApiOperation(value = "Updates Transaction state",
            notes = "Update existing transaction record. Could generate 404 if no matching record is found",
            response = Transaction.class)
    public Response updateTransaction(
            @PathParam("transactionId") UUID transactionId,
            @PathParam("state") TransactionState state
    ) {

        Transaction transaction;
        try {
            transaction = transactionService.updateState(transactionId, state);
        } catch (ElementNotFoundException enf) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(enf.getMessage())
                    .build();
        }

        return Response.status(Response.Status.OK).entity(transaction).build();
    }
}
