package com.revolut.service;

import com.google.inject.Inject;
import com.revolut.model.Account;
import com.revolut.model.Transaction;
import com.revolut.service.data.DataSource;
import com.revolut.service.exception.DuplicateElementException;
import com.revolut.service.exception.ElementNotFoundException;
import com.revolut.service.exception.InvalidTransactionAmountException;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class BankAccountService implements AccountService {

    @Inject
    private DataSource<Account> accountDataSource;

    @Override
    public Account get(UUID accountId) throws ElementNotFoundException {
        return accountDataSource.get(accountId);
    }

    @Override
    public List<Account> getAll() {
        return accountDataSource.getAll();
    }

    @Override
    public void store(Account account) throws DuplicateElementException {
        accountDataSource.add(account);
    }

    /**
     * Processes a transaction for given accounts and amount.
     * This method is synchronised and thread-safe.
     * @param transaction payment transaction
     * @throws ElementNotFoundException no matching account was found
     * @throws InvalidTransactionAmountException insufficient fund on account
     */
    @Override
    public synchronized void applyTransaction(Transaction transaction) throws ElementNotFoundException, InvalidTransactionAmountException {
        Account fromAccount = accountDataSource.get(transaction.getFromAccount().getId());
        Account toAccount = accountDataSource.get(transaction.getToAccount().getId());

        if (fromAccount.getBalance().compareTo(transaction.getAmount()) < 0) {
            throw new InvalidTransactionAmountException("Account has insufficient funds to process ");
        }

        BigDecimal amount = transaction.getAmount();
        fromAccount.subtractFromBalance(amount);
        toAccount.addToBalance(amount);
    }

    /**
     * Permanently delete a bank account.
     * This method is synchronised and thread-safe.
     * @param accountId account identifier
     * @throws ElementNotFoundException account doesn't exists
     */
    @Override
    public synchronized void delete(UUID accountId) throws ElementNotFoundException {
        accountDataSource.remove(accountId);
    }
}
