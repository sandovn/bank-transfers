package com.revolut.service;

import com.google.inject.Inject;
import com.revolut.model.Account;
import com.revolut.model.Transaction;
import com.revolut.model.transaction.TransactionState;
import com.revolut.service.data.DataSource;
import com.revolut.service.exception.DuplicateElementException;
import com.revolut.service.exception.ElementNotFoundException;
import com.revolut.service.exception.InvalidTransactionAmountException;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class PaymentTransactionService implements TransactionService {

    /**
     * Maximum transaction amount. If exceeded we'll throw exception.
     */
    private static final BigDecimal MAX_TRANSACTION_AMOUNT = BigDecimal.valueOf(10000);

    private DataSource<Transaction> transactionDataSource;

    private AccountService accountService;

    @Inject
    PaymentTransactionService(DataSource<Transaction> transactionDataSource, AccountService accountSource) {
        this.transactionDataSource = transactionDataSource;
        this.accountService = accountSource;
    }

    @Override
    public Transaction get(UUID transactionId) throws ElementNotFoundException {
        return transactionDataSource.get(transactionId);
    }

    @Override
    public List<Transaction> getAll() {
        return transactionDataSource.getAll();
    }

    /**
     * Process and store a transaction against two bank accounts and given amount.
     * This method is synchronised and thread-safe.
     * @param transaction to be processed
     * @throws DuplicateElementException existing transaction
     * @throws InvalidTransactionAmountException transaction amount is negative or above max value
     * @throws ElementNotFoundException account is not present in data source
     */
    @Override
    public synchronized void store(
            Transaction transaction
    ) throws DuplicateElementException, InvalidTransactionAmountException, ElementNotFoundException {
        if (transaction.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            throw new InvalidTransactionAmountException("Transaction amount cannot be 0 or negative");
        }
        if (transaction.getAmount().compareTo(MAX_TRANSACTION_AMOUNT) > 0) {
            throw new InvalidTransactionAmountException(
                    "Transaction amount exceeds the maximum allowed: " + MAX_TRANSACTION_AMOUNT.toString()
            );
        }

        accountService.applyTransaction(transaction);
        transactionDataSource.add(transaction);
    }

    /**
     * Process and store a transaction against two bank accounts and given amount.
     * This method is synchronised and thread-safe.
     * @param fromAccountId funds origination account
     * @param toAccountId funds destination account
     * @param amount total transaction amount
     * @return Transaction
     * @throws DuplicateElementException
     * @throws ElementNotFoundException
     * @throws InvalidTransactionAmountException
     */
    @Override
    public synchronized Transaction store(
            UUID fromAccountId, UUID toAccountId, BigDecimal amount
    ) throws DuplicateElementException, ElementNotFoundException, InvalidTransactionAmountException {
        Account fromAccount, toAccount;
        try {
            fromAccount = accountService.get(fromAccountId);
        } catch (ElementNotFoundException e) {
            throw new ElementNotFoundException("There is no matching 'from' account with ID: " + fromAccountId.toString(), e);
        }

        try {
            toAccount = accountService.get(toAccountId);
        } catch (ElementNotFoundException e) {
            throw new ElementNotFoundException("There is no matching 'to' account with ID: " + toAccountId.toString(), e);
        }

        Transaction transaction = new Transaction(TransactionState.FULFILLED, fromAccount, toAccount, amount);
        this.store(transaction);
        return transaction;
    }

    @Override
    public Transaction updateState(UUID transactionId, TransactionState state) throws ElementNotFoundException {
        Transaction transaction = transactionDataSource.get(transactionId);
        transaction.setState(state);
        return transaction;
    }

    @Override
    public void delete(UUID transactionId) throws ElementNotFoundException {
        transactionDataSource.remove(transactionId);
    }
}
