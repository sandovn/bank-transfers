package com.revolut.service;

import com.revolut.model.Account;
import com.revolut.model.Transaction;
import com.revolut.service.exception.DuplicateElementException;
import com.revolut.service.exception.ElementNotFoundException;
import com.revolut.service.exception.InvalidTransactionAmountException;

import java.util.List;
import java.util.UUID;

public interface AccountService {

    Account get(UUID accountId) throws ElementNotFoundException;

    List<Account> getAll();

    /**
     * Persist transaction in internal data source
     * @param accountId account identifier
     * @throws DuplicateElementException if account is already existing in the internal data source
     */
    void store(Account accountId) throws DuplicateElementException;

    /**
     * Applies a transaction for two accounts
     * @param transaction payment transaction
     * @throws ElementNotFoundException transaction or account are not present in data source
     * @throws InvalidTransactionAmountException 0 or negative transaction amount
     */
    void applyTransaction(Transaction transaction) throws ElementNotFoundException, InvalidTransactionAmountException;

    /**
     * Delete an exisitng bank account
     * @param accountId account identifier
     * @throws ElementNotFoundException if there is no matching account in data source
     */
    void delete(UUID accountId) throws ElementNotFoundException;
}
