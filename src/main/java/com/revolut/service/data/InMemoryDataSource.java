package com.revolut.service.data;

import com.google.inject.Singleton;
import com.revolut.model.IdentifiableEntity;
import com.revolut.service.exception.DuplicateElementException;
import com.revolut.service.exception.ElementNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * In-memory storage backed by a Map. This data source is thread safe.
 * @param <T> identifiable element with UUID
 */
@Singleton
public class InMemoryDataSource<T extends IdentifiableEntity> implements DataSource<T> {

    /**
     * We need synchronised map just to make sure that
     * multiple requests retrieving/modifying same object
     * will get correct data.
     */
    private Map<UUID, T> elements = new ConcurrentHashMap<>();


    @Override
    public T get(UUID id) throws ElementNotFoundException {
        if (!elements.containsKey(id)) {
            throw new ElementNotFoundException("There is no matching element with ID: " + id.toString());
        }

        return elements.get(id);
    }

    @Override
    public List<T> getAll() {
        return new ArrayList<>(elements.values());
    }

    @Override
    public void add(T value) throws DuplicateElementException {
        UUID id = value.getId();
        if (elements.containsKey(id)) {
            throw new DuplicateElementException("An element with ID: " + id.toString() + " already exists");
        }

        elements.put(id, value);
    }

    @Override
    public void remove(UUID id) throws ElementNotFoundException {
        if (!elements.containsKey(id)) {
            throw new ElementNotFoundException("There is no matching element with ID: " + id.toString());
        }

        elements.remove(id);
    }
}
