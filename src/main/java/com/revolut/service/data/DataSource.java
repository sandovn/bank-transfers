package com.revolut.service.data;

import com.revolut.model.IdentifiableEntity;
import com.revolut.service.exception.DuplicateElementException;
import com.revolut.service.exception.ElementNotFoundException;

import java.util.List;
import java.util.UUID;

/**
 * Generic data source to store a collection of the entities.
 * Each object must be identifiable by {@link UUID} and must
 * be unique in the underlying storage.
 * @param <T>
 */
public interface DataSource<T extends IdentifiableEntity> {

    /**
     * Retrieve single element by ID.
     * @param id element identifier
     * @return the element
     * @throws ElementNotFoundException if the element cannot be found
     */
    T get(UUID id) throws ElementNotFoundException;

    /**
     * Get all stored elements.
     * @return list of elements
     */
    List<T> getAll();

    /**
     * Add new element to underlying data storage.
     * @param value element to be added
     * @throws DuplicateElementException if the element already exists
     */
    void add(T value) throws DuplicateElementException;

    /**
     * Delete element from the data storage.
     * @param id of the element to be removed
     * @throws ElementNotFoundException if the element cannot be found
     */
    void remove(UUID id) throws ElementNotFoundException;
}
