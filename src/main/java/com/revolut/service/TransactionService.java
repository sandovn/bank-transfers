package com.revolut.service;

import com.revolut.model.Transaction;
import com.revolut.model.transaction.TransactionState;
import com.revolut.service.exception.DuplicateElementException;
import com.revolut.service.exception.ElementNotFoundException;
import com.revolut.service.exception.InvalidTransactionAmountException;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

/**
 * Main payment transaction service that handles
 * payments from one account to another and also
 * storing all transactions to internal storage.
 */
public interface TransactionService {

    Transaction get(UUID transactionId) throws ElementNotFoundException;

    List<Transaction> getAll();

    void store(
            Transaction transaction
    ) throws DuplicateElementException, InvalidTransactionAmountException, ElementNotFoundException;

    Transaction store(
            UUID fromAccount, UUID toAccount, BigDecimal amount
    ) throws DuplicateElementException, ElementNotFoundException, InvalidTransactionAmountException;

    Transaction updateState(UUID transactionId, TransactionState state) throws ElementNotFoundException;

    void delete(UUID transactionId) throws ElementNotFoundException;
}
