package com.revolut.service.exception;

/**
 * Invalid transaction amount, e.g. Account balance has lower
 * amount or the transaction amount is zero or negative.
 */
public class InvalidTransactionAmountException extends Exception {

    public InvalidTransactionAmountException() {}

    public InvalidTransactionAmountException(String message) {
        super(message);
    }

    public InvalidTransactionAmountException(String message, Throwable exception) {
        super(message, exception);
    }
}
