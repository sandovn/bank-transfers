package com.revolut.service.exception;

public class DuplicateElementException extends Exception {

    public DuplicateElementException() {}

    public DuplicateElementException(String message) {
        super(message);
    }
}
