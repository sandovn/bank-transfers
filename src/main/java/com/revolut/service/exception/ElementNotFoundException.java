package com.revolut.service.exception;

/**
 * No matching element was found in the data source
 */
public class ElementNotFoundException extends Exception {

    public ElementNotFoundException() {}

    public ElementNotFoundException(String message) {
        super(message);
    }

    public ElementNotFoundException(String message, Throwable exception) {
        super(message, exception);
    }
}
