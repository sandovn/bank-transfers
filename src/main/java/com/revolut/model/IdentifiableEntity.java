package com.revolut.model;

import java.util.UUID;

/**
 * Identifiable entity through UUID
 */
public interface IdentifiableEntity {

    UUID getId();
}
