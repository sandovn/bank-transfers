package com.revolut.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.revolut.service.exception.InvalidTransactionAmountException;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * Bank account entity
 */
public class Account implements IdentifiableEntity {

    private UUID id;
    private String name;
    private String number;
    private String sortCode;
    private BigDecimal balance;
    private boolean active;

    public Account(String name, String number, String sortCode, boolean active) {
        this(UUID.randomUUID(), name, number, sortCode, BigDecimal.ZERO, active);
    }

    public Account(UUID uuid, String name, String number, String sortCode, boolean active) {
        this(uuid, name, number, sortCode, BigDecimal.ZERO, active);
    }

    @JsonCreator
    public Account(
            @JsonProperty("id") UUID id,
            @JsonProperty("name") String name,
            @JsonProperty("number") String number,
            @JsonProperty("sortCode") String sortCode,
            @JsonProperty("ballance") BigDecimal balance,
            @JsonProperty("active") boolean active
    ) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.sortCode = sortCode;
        this.balance = balance;
        this.active = active;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Decrement account balance with given amount.
     * This method is synchronised and thread-safe.
     * @param amount to be substracted from account balance
     * @throws InvalidTransactionAmountException invalid amount
     */
    public synchronized void subtractFromBalance(BigDecimal amount) throws InvalidTransactionAmountException {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new InvalidTransactionAmountException("Subtracting negative amount from account");
        }
        if (balance.compareTo(amount) < 0) {
            throw new InvalidTransactionAmountException("Account has insufficient funds");
        }
        balance = balance.subtract(amount);
    }

    /**
     * Increment account balance with given amount.
     * This method is synchronised and thread-safe.
     * @param amount to be added to account balance
     * @throws InvalidTransactionAmountException invalid amount
     */
    public synchronized void addToBalance(BigDecimal amount) throws InvalidTransactionAmountException {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new InvalidTransactionAmountException("Subtracting negative amount from account");
        }
        balance = balance.add(amount);
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;
        Account account = (Account) o;
        return Objects.equal(getId(), account.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
