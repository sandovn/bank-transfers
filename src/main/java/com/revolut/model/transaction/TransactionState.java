package com.revolut.model.transaction;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Possible payment transaction states.
 */
public enum TransactionState {
    PENDING("pending"),
    FULFILLED("fullfilled"),
    CANCELLED("cancelled"),
    CHARGEBACK("chargeback"),
    PROCESSING("processing");

    private final String value;

    TransactionState(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @JsonValue
    @Override
    public String toString() {
        return value;
    }
}
