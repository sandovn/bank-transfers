package com.revolut.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Objects;
import com.revolut.config.serialization.MoneySerializer;
import com.revolut.model.transaction.TransactionState;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * Payment transaction entity
 */
public class Transaction implements IdentifiableEntity {

    private UUID id;

    private TransactionState state;
    private Account fromAccount;
    private Account toAccount;

    @JsonSerialize(using=MoneySerializer.class)
    private BigDecimal amount;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private Date date;

    public Transaction(TransactionState state, Account fromAccount, Account toAccount, BigDecimal amount) {
        this(UUID.randomUUID(), state, fromAccount, toAccount, amount, new Date());
    }

    public Transaction(TransactionState state, Account fromAccount, Account toAccount, BigDecimal amount, Date date) {
        this(UUID.randomUUID(), state, fromAccount, toAccount, amount, date);
    }

    @JsonCreator
    public Transaction(
            @JsonProperty("id") UUID id,
            @JsonProperty("state") TransactionState state,
            @JsonProperty("fromAccount") Account fromAccount,
            @JsonProperty("toAccount") Account toAccount,
            @JsonProperty("amount") BigDecimal amount,
            @JsonProperty("date") Date date
    ) {
        this.id = id;
        this.state = state;
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
        this.date = date;
    }

    public UUID getId() {
        return id;
    }

    public void setState(TransactionState state) {
        this.state = state;
    }

    public TransactionState getState() {
        return state;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    @JsonSerialize(using=MoneySerializer.class)
    public BigDecimal getAmount() {
        return amount;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;
        Transaction that = (Transaction) o;
        return Objects.equal(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
