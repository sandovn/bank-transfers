package com.revolut;

import com.google.inject.servlet.GuiceFilter;
import com.revolut.config.ServletContextListener;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.util.EnumSet;

import static org.glassfish.jersey.servlet.ServletProperties.FILTER_STATIC_CONTENT_REGEX;

public class App {

    public static void main(String[] args) throws Exception {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setResourceBase(System.getProperty("user.dir"));
        context.setContextPath("/");

        Server jettyServer = new Server(8080);
        jettyServer.setHandler(context);

        // Serve static content from /tmp
        ServletHolder defaultServlet = context.addServlet(DefaultServlet.class, "/docs/*");
        defaultServlet.setInitParameter(FILTER_STATIC_CONTENT_REGEX, "/");
        defaultServlet.setInitOrder(0);

        ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/api/v1/*");
        jerseyServlet.setInitOrder(1);

        context.addEventListener(new ServletContextListener());
        context.addFilter(GuiceFilter.class, "/api/v1/*",
                EnumSet.of(javax.servlet.DispatcherType.REQUEST,javax.servlet.DispatcherType.ASYNC));

        try {
            jettyServer.start();
            jettyServer.join();
        } catch (Exception e){
            e.printStackTrace();
            jettyServer.stop();
            jettyServer.destroy();
        }
    }
}
