package com.revolut.service;

import com.revolut.model.Account;
import com.revolut.model.Transaction;
import com.revolut.model.transaction.TransactionState;
import com.revolut.service.data.InMemoryDataSource;
import com.revolut.service.exception.DuplicateElementException;
import com.revolut.service.exception.ElementNotFoundException;
import com.revolut.service.exception.InvalidTransactionAmountException;
import com.revolut.utils.AccountUtils;
import com.revolut.utils.TransactionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PaymentTransactionServiceTest {

    private static final UUID EXISTING_TRANSACTION = UUID.fromString("ce4da507-0b1a-4a9a-a97a-351a70e638e6");
    private static final UUID NON_EXISTING_TRANSACTION = UUID.fromString("14d50b01-ea16-409d-b4af-5aaa7b81e040");
    private static final UUID EXISTING_FROM_ACCOUNT = UUID.fromString("99ed1e22-97cc-4fc9-a568-7ef2a307b57c");
    private static final UUID NON_EXISTING_FROM_ACCOUNT = UUID.fromString("39274609-1187-44f4-b83f-c76057705d71");
    private static final UUID EXISTING_TO_ACCOUNT = UUID.fromString("05a48c1a-4ff9-4313-b881-779922eaf4d1");
    private static final UUID NON_EXISTING_TO_ACCOUNT = UUID.fromString("c47e476b-0c63-4edc-a2e9-0022e03dce61");

    private PaymentTransactionService service;

    @Mock
    private InMemoryDataSource<Transaction> transactionDataSource;

    @Mock
    private AccountService accountService;

    @Before
    public void setUp() {
        service = new PaymentTransactionService(transactionDataSource, accountService);
    }

    @Test
    public void get_existing_transaction() throws Exception {
        Transaction transaction = TransactionUtils.generateRandomTransaction();
        when(transactionDataSource.get(EXISTING_TRANSACTION)).thenReturn(transaction);
        Transaction newTransaction = service.get(EXISTING_TRANSACTION);
        assertNotNull(newTransaction);
        assertEquals(transaction, newTransaction);
    }

    @Test(expected = ElementNotFoundException.class)
    public void get_non_existing_transaction_throws_exception() throws Exception {
        when(transactionDataSource.get(NON_EXISTING_TRANSACTION)).thenThrow(new ElementNotFoundException());
        service.get(NON_EXISTING_TRANSACTION);
    }

    @Test
    public void get_all_existing_transactions() {
        List<Transaction> transactionList = Arrays.asList(TransactionUtils.generateRandomTransaction(), TransactionUtils.generateRandomTransaction());
        when(transactionDataSource.getAll()).thenReturn(transactionList);
        List<Transaction> transactionsRetrieved = service.getAll();
        assertNotNull(transactionsRetrieved);
        assertEquals(transactionList, transactionsRetrieved);
    }

    @Test
    public void store_transaction_successfully() throws Exception {
        Transaction transaction = TransactionUtils.generateRandomTransaction();
        service.store(transaction);
        verify(transactionDataSource, times(1)).add(transaction);
    }

    @Test(expected = DuplicateElementException.class)
    public void store_transaction_duplicate_throws_exception() throws Exception {
        Transaction transaction = TransactionUtils.generateRandomTransaction();
        doThrow(DuplicateElementException.class).when(transactionDataSource).add(transaction);
        service.store(transaction);
    }

    @Test(expected = InvalidTransactionAmountException.class)
    public void store_transaction_with_zero_amount_throws_exception() throws Exception {
        Transaction transaction = new Transaction(
                TransactionState.FULFILLED,
                AccountUtils.generateRandomAccount(),
                AccountUtils.generateRandomAccount(),
                BigDecimal.ZERO
        );
        service.store(transaction);
    }

    @Test(expected = InvalidTransactionAmountException.class)
    public void store_transaction_with_negative_amount_throws_exception() throws Exception {
        Transaction transaction = new Transaction(
                TransactionState.FULFILLED,
                AccountUtils.generateRandomAccount(),
                AccountUtils.generateRandomAccount(),
                BigDecimal.valueOf(-22)
        );
        service.store(transaction);
    }

    @Test(expected = InvalidTransactionAmountException.class)
    public void store_transaction_with_account_with_insufficient_amount_throws_exception() throws Exception {
        Account fromAccount = AccountUtils.generateRandomAccount();
        fromAccount.setBalance(BigDecimal.ZERO);
        Account toAccount = AccountUtils.generateRandomAccount();
        Transaction transaction = new Transaction(TransactionState.FULFILLED, fromAccount, toAccount, BigDecimal.valueOf(22));

        doThrow(InvalidTransactionAmountException.class).when(accountService).applyTransaction(any(Transaction.class));

        service.store(transaction);
    }

    @Test(expected = InvalidTransactionAmountException.class)
    public void store_transaction_exceeding_max_throws_exception() throws Exception {
        Account fromAccount = AccountUtils.generateRandomAccount();
        Account toAccount = AccountUtils.generateRandomAccount();
        Transaction transaction = new Transaction(TransactionState.FULFILLED, fromAccount, toAccount, BigDecimal.valueOf(10001));
        service.store(transaction);
    }

    @Test
    public void store_from_to_account_and_amount_success() throws Exception {
        Account fromAccount = AccountUtils.generateRandomAccount();
        Account toAccount = AccountUtils.generateRandomAccount();
        when(accountService.get(EXISTING_FROM_ACCOUNT)).thenReturn(fromAccount);
        when(accountService.get(EXISTING_TO_ACCOUNT)).thenReturn(toAccount);

        BigDecimal amount = BigDecimal.valueOf(22);
        Transaction storedTransaction = service.store(EXISTING_FROM_ACCOUNT, EXISTING_TO_ACCOUNT, amount);

        assertNotNull(storedTransaction);
        assertEquals(fromAccount, storedTransaction.getFromAccount());
        assertEquals(toAccount, storedTransaction.getToAccount());
        assertEquals(amount, storedTransaction.getAmount());
    }

    @Test(expected = InvalidTransactionAmountException.class)
    public void store_from_to_account_and_zero_amount_throws_exception() throws Exception {
        Account fromAccount = AccountUtils.generateRandomAccount();
        Account toAccount = AccountUtils.generateRandomAccount();
        when(accountService.get(EXISTING_FROM_ACCOUNT)).thenReturn(fromAccount);
        when(accountService.get(EXISTING_TO_ACCOUNT)).thenReturn(toAccount);

        BigDecimal amount = BigDecimal.ZERO;
        service.store(EXISTING_FROM_ACCOUNT, EXISTING_TO_ACCOUNT, amount);
    }

    @Test(expected = InvalidTransactionAmountException.class)
    public void store_from_to_account_and_negative_amount_throws_exception() throws Exception {
        Account fromAccount = AccountUtils.generateRandomAccount();
        Account toAccount = AccountUtils.generateRandomAccount();
        when(accountService.get(EXISTING_FROM_ACCOUNT)).thenReturn(fromAccount);
        when(accountService.get(EXISTING_TO_ACCOUNT)).thenReturn(toAccount);

        BigDecimal amount = BigDecimal.valueOf(-22);
        service.store(EXISTING_FROM_ACCOUNT, EXISTING_TO_ACCOUNT, amount);
    }

    @Test(expected = InvalidTransactionAmountException.class)
    public void store_from_to_account_and_insufficient_amount_throws_exception() throws Exception {
        Account fromAccount = AccountUtils.generateRandomAccount();
        fromAccount.setBalance(BigDecimal.ZERO);
        Account toAccount = AccountUtils.generateRandomAccount();
        when(accountService.get(EXISTING_FROM_ACCOUNT)).thenReturn(fromAccount);
        when(accountService.get(EXISTING_TO_ACCOUNT)).thenReturn(toAccount);
        doThrow(InvalidTransactionAmountException.class).when(accountService).applyTransaction(any(Transaction.class));

        BigDecimal amount = BigDecimal.valueOf(22);
        service.store(EXISTING_FROM_ACCOUNT, EXISTING_TO_ACCOUNT, amount);
    }

    @Test(expected = ElementNotFoundException.class)
    public void store_non_existing_from_account_throws_exception() throws Exception {
        when(accountService.get(NON_EXISTING_FROM_ACCOUNT)).thenThrow(ElementNotFoundException.class);

        service.store(NON_EXISTING_FROM_ACCOUNT, EXISTING_TO_ACCOUNT, BigDecimal.valueOf(22));
    }

    @Test(expected = ElementNotFoundException.class)
    public void store_non_existing_to_account_throws_exception() throws Exception {
        when(accountService.get(EXISTING_FROM_ACCOUNT)).thenReturn(AccountUtils.generateRandomAccount());
        when(accountService.get(NON_EXISTING_TO_ACCOUNT)).thenThrow(ElementNotFoundException.class);

        service.store(EXISTING_FROM_ACCOUNT, NON_EXISTING_TO_ACCOUNT, BigDecimal.valueOf(22));
    }

    @Test
    public void update_transaction_state_successful() throws Exception {
        Transaction originalTransaction = TransactionUtils.generateRandomTransaction();
        when(transactionDataSource.get(EXISTING_TRANSACTION)).thenReturn(originalTransaction);
        Transaction storedTransaction = service.updateState(EXISTING_TRANSACTION, TransactionState.CHARGEBACK);

        assertNotNull(storedTransaction);
        assertEquals(TransactionState.CHARGEBACK, storedTransaction.getState());
        assertEquals(originalTransaction, storedTransaction);
    }

    @Test(expected = ElementNotFoundException.class)
    public void update_transaction_state_non_existing_transaction_throws_exception() throws Exception {
        when(transactionDataSource.get(NON_EXISTING_TRANSACTION)).thenThrow(ElementNotFoundException.class);
        service.updateState(NON_EXISTING_TRANSACTION, TransactionState.CHARGEBACK);
    }
}
