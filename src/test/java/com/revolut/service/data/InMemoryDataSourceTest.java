package com.revolut.service.data;

import com.revolut.model.Account;
import com.revolut.service.exception.DuplicateElementException;
import com.revolut.service.exception.ElementNotFoundException;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class InMemoryDataSourceTest {

    private static final UUID ACCOUNT_ONE_ID = UUID.fromString("6e874d62-d23c-4b84-a8dd-9ef6426dbb37");
    private static final Account ACCOUNT_ONE = new Account(
            ACCOUNT_ONE_ID,
            "John Doe",
            "045678987",
            "20-87-10",
            true
    );
    private static final UUID ACCOUNT_TWO_ID = UUID.fromString("84d490fc-882d-456a-b921-45bac8457f84");
    private static final Account ACCOUNT_TWO = new Account(
            ACCOUNT_TWO_ID,
            "Damian James",
            "034567986",
            "20-70-12",
            true
    );

    private static InMemoryDataSource<Account> dataSource;

    @Before
    public void setUp() throws Exception {
        dataSource = new InMemoryDataSource<>();
        dataSource.add(ACCOUNT_ONE);
        dataSource.add(ACCOUNT_TWO);
    }

    @Test
    public void get_existing_element_returns_result() throws Exception {
        Account result1 = dataSource.get(ACCOUNT_ONE_ID);
        Account result2 = dataSource.get(ACCOUNT_TWO_ID);

        assertEquals(result1, ACCOUNT_ONE);
        assertEquals(result2, ACCOUNT_TWO);
    }

    @Test(expected = ElementNotFoundException.class)
    public void get_non_existing_element_throws_not_found_exception() throws Exception {
        dataSource.get(UUID.randomUUID());
    }

    @Test
    public void get_all_returns_list_of_elements() {
        List<Account> list = dataSource.getAll();

        assertEquals(2, list.size());
        assertTrue(list.contains(ACCOUNT_ONE));
        assertTrue(list.contains(ACCOUNT_TWO));
    }

    @Test(expected = DuplicateElementException.class)
    public void adding_element_with_duplicate_id_throws_exception() throws Exception {
        dataSource.add(ACCOUNT_ONE);
    }

    @Test
    public void adding_new_element_stores_it_successfully_in_data_source() throws Exception {
        UUID uuid = UUID.fromString("9ac77d62-da2c-4b84-a867-9ef6426dbb37");
        Account newAccount = new Account(uuid,"Patrick Derby","015788977","20-87-10",true);
        dataSource.add(newAccount);

        assertEquals(newAccount, dataSource.get(uuid));
        assertEquals(3, dataSource.getAll().size());
    }

    @Test(expected = ElementNotFoundException.class)
    public void removing_non_existing_account_throws_exception() throws Exception {
        dataSource.remove(UUID.randomUUID());
    }

    @Test(expected = ElementNotFoundException.class)
    public void removing_existing_account_reduces_total_count() throws Exception {
        UUID uuid = UUID.fromString("9ac77d62-da2c-4b84-a867-9ef6426dbb37");
        dataSource.remove(uuid);

        List<Account> list = dataSource.getAll();

        assertEquals(2, list.size());
        assertTrue(list.contains(ACCOUNT_ONE));
        assertTrue(list.contains(ACCOUNT_TWO));
    }
}
