package com.revolut.utils;

import com.revolut.model.Account;

import java.util.Random;

public class AccountUtils {

    public static Account generateRandomAccount() {
        Random rand = new Random();
        Integer randomAccountNumber = rand.nextInt((99999999 - 10000000) + 1) + 10000000;
        return new Account("John Doe", randomAccountNumber.toString(), "12-34-56", true);
    }
}
