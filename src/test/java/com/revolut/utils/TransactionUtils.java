package com.revolut.utils;

import com.revolut.model.Transaction;
import com.revolut.model.transaction.TransactionState;

import java.math.BigDecimal;

public class TransactionUtils {

    public static Transaction generateRandomTransaction() {
        return new Transaction(
                TransactionState.FULFILLED,
                AccountUtils.generateRandomAccount(),
                AccountUtils.generateRandomAccount(),
                BigDecimal.ONE
        );
    }

}
