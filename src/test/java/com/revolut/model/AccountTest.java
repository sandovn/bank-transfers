package com.revolut.model;

import com.revolut.service.exception.InvalidTransactionAmountException;
import com.revolut.utils.AccountUtils;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class AccountTest {

    @Test
    public void set_balance_is_correctly_reflected() {
        Account account = AccountUtils.generateRandomAccount();
        assertEquals(account.getBalance(), BigDecimal.ZERO);
        account.setBalance(BigDecimal.valueOf(20));
        assertEquals(account.getBalance(), BigDecimal.valueOf(20));
    }

    @Test
    public void subtract_from_balance_is_correctly_reflected() throws Exception {
        Account account = AccountUtils.generateRandomAccount();
        account.setBalance(BigDecimal.valueOf(20));
        account.subtractFromBalance(BigDecimal.valueOf(10));
        assertEquals(account.getBalance(), BigDecimal.valueOf(10));
    }

    @Test(expected = InvalidTransactionAmountException.class)
    public void subtract_negative_amount_from_balance_throws_exception() throws Exception {
        Account account = AccountUtils.generateRandomAccount();
        account.subtractFromBalance(BigDecimal.valueOf(-10));
    }

    @Test(expected = InvalidTransactionAmountException.class)
    public void subtract_larger_amount_from_balance_throws_exception() throws Exception {
        Account account = AccountUtils.generateRandomAccount();
        account.setBalance(BigDecimal.valueOf(20));
        account.subtractFromBalance(BigDecimal.valueOf(30));
    }

    @Test
    public void add_to_balance_is_correctly_reflected() throws Exception {
        Account account = AccountUtils.generateRandomAccount();
        account.setBalance(BigDecimal.valueOf(20));
        account.addToBalance(BigDecimal.valueOf(10));
        assertEquals(account.getBalance(), BigDecimal.valueOf(30));
    }

    @Test(expected = InvalidTransactionAmountException.class)
    public void add_to_balance_negative_value_throws_exception() throws Exception {
        Account account = AccountUtils.generateRandomAccount();
        account.setBalance(BigDecimal.valueOf(20));
        account.addToBalance(BigDecimal.valueOf(-10));
    }
}
