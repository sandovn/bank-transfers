package com.revolut.controllers;

import com.revolut.model.Transaction;
import com.revolut.service.TransactionService;
import com.revolut.service.exception.DuplicateElementException;
import com.revolut.service.exception.ElementNotFoundException;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.revolut.utils.TransactionUtils.generateRandomTransaction;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.mockito.Mockito.when;

public class TransactionControllerTest extends JerseyTest {

    @Mock
    private static TransactionService transactionService = Mockito.mock(TransactionService.class);

    @Override
    public Application configure() {
        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);
        return new ResourceConfig().register(new TransactionController(transactionService));
    }

    @Test
    public void test_get_all_transactions_generates_list() {
        when(transactionService.getAll()).thenReturn(Arrays.asList(
                generateRandomTransaction(),
                generateRandomTransaction(),
                generateRandomTransaction(),
                generateRandomTransaction(),
                generateRandomTransaction()
        ));
        Response output = target("/transactions").request().get();
        assertEquals("Should return status 200", 200, output.getStatus());
        assertNotNull("Should return list", output.getEntity());
        assertNotNull("Should return list", output.getEntity());
        assertEquals("Should have 5 transactions", 5, output.readEntity(List.class).size());
    }

    @Test
    public void test_get_by_ivalid_uuid_generates_404() {
        Response output = target("/transactions/this-is-not-uuid").request().get();
        assertEquals("Should return status 404", 404, output.getStatus());
    }

    @Test
    public void test_non_existing_transaction_generates_404() throws Exception {
        UUID uuid = UUID.fromString("3d24f58c-1681-42f8-8133-28771f639c3e");
        when(transactionService.get(uuid)).thenThrow(new ElementNotFoundException("no such transaction"));

        Response output = target("/transactions/3d24f58c-1681-42f8-8133-28771f639c3e").request().get();
        assertEquals("Should return status 404", 404, output.getStatus());
    }

    @Test
    public void test_existing_transaction_generates_success_response_with_data() throws Exception {
        Transaction transaction = generateRandomTransaction();
        when(transactionService.get(transaction.getId())).thenReturn(transaction);

        Response output = target("/transactions/"+transaction.getId().toString()).request().get();
        assertEquals("Should return status 200", 200, output.getStatus());
        assertEquals("Should have transaction", transaction, output.readEntity(Transaction.class));
    }

    @Test
    public void test_create_new_transaction_generates_created_response() throws Exception {
        Transaction transaction = generateRandomTransaction();
        UUID fromAccountId = transaction.getFromAccount().getId();
        UUID toAccountId = transaction.getFromAccount().getId();
        BigDecimal amount = BigDecimal.valueOf(25.47);

        when(transactionService.store(fromAccountId, toAccountId, amount)).thenReturn(transaction);
        Response output = target(String.format(
                "/transactions/%s/%s/%s", fromAccountId.toString(), toAccountId.toString(), amount.toString()
        )).request().post(null);

        assertEquals("Should return status 201", 201, output.getStatus());
        assertNotNull("Should return notification", output.getEntity());
        assertEquals("Should have transaction", transaction, output.readEntity(Transaction.class));
    }

    @Test
    public void test_create_new_transaction_and_non_matching_account_generates_bad_request() throws Exception {
        Transaction transaction = generateRandomTransaction();
        UUID fromAccountId = transaction.getFromAccount().getId();
        UUID toAccountId = transaction.getFromAccount().getId();
        BigDecimal amount = BigDecimal.valueOf(38.47);

        when(transactionService.store(fromAccountId, toAccountId, amount)).thenThrow(new ElementNotFoundException("no matching account"));
        Response output = target(String.format(
                "/transactions/%s/%s/%s", fromAccountId.toString(), toAccountId.toString(), amount.toString()
        )).request().post(null);

        assertEquals("Should return status 400", 400, output.getStatus());
    }

    @Test
    public void test_create_request_existing_transaction_generates_internal_error() throws Exception {
        Transaction transaction = generateRandomTransaction();
        UUID fromAccountId = transaction.getFromAccount().getId();
        UUID toAccountId = transaction.getFromAccount().getId();
        BigDecimal amount = BigDecimal.valueOf(20.01);

        when(transactionService.store(fromAccountId, toAccountId, amount)).thenThrow(new DuplicateElementException("duplicate transaction"));
        Response output = target(String.format(
                "/transactions/%s/%s/%s", fromAccountId.toString(), toAccountId.toString(), amount.toString()
        )).request().post(null);

        assertEquals("Should return status 500", 500, output.getStatus());
    }

}
