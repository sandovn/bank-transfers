Payment Transfer Service
===

This is a payment transfer service that allows us to handle Payment Transactions and Bank Accounts.

The whole service is based around the idea that there are various clients and each client has one single 
bank account that has balance, account number and sort code. From there you can execute a transaction and
we'll apply relevant validations and checks on the sent transaction.

Another limitation of the service is that each transaction cannot exceed 10000. Currently there is no currency 
handling we just have balance amount though that can be extended and we could have fully implemented Monetary 
entities.

Requirements
---
- Java
- Maven

Build
---
```
# mvn clean package
# mvn exec:java
```

Run project
---
By default the project will be running on `http://localhost:8080`

The documentation of the service is based here [http://localhost:8080/docs](http://localhost:8080/docs)

The documentation is being generated through Swagger Annotations and Swagger UI which could be used to 
send real requests to the service through UI.

If you decide you can also use direct access though the following requests:

- Seed random data in data source: `curl -X POST "http://localhost:8080/api/v1/data"`
- Fetch all stored transactions: `curl -X GET "http://localhost:8080/api/v1/transactions"`
- Fetch a single transaction: `curl -X GET "http://localhost:8080/api/v1/transactions/bd7d0f1b-9295-4dae-97f2-9b916bd0e32d"`
- Create new transaction for known accounts: `curl -X POST "http://localhost:8080/api/v1/transactions/19f5420d-8a3b-4bbb-ab9a-447db018281b/d763c031-2cb7-44f5-aaef-6f481d622576/200"`
- Change transaction's state: `curl -X PATCH "http://localhost:8080/api/v1/transactions/f788b919-b2cf-4024-a435-984bab77ed50/PENDING"`


Examples
---
Transaction:
```json
{
  "id": "f788b919-b2cf-4024-a435-984bab77ed50",
  "state": "fullfilled",
  "fromAccount": {
    "id": "241d9aa8-cb6e-43ba-87d1-9a30fd85e12c",
    "name": "John Doe",
    "number": "087659642",
    "sortCode": "23-01-03",
    "active": true,
    "balance": 245.81
  },
  "toAccount": {
    "id": "40283cf8-c5e7-4c10-82c0-8a4b38920c35",
    "name": "Bob Dilan",
    "number": "2839403725",
    "sortCode": "23-90-53",
    "active": true,
    "balance": 1457.59
  },
  "amount": "200.00",
  "date": "2018-03-09 02:29:45.730"
}
```

Account:
```json
{
  "id": "40283cf8-c5e7-4c10-82c0-8a4b38920c35",
  "name": "Bob Dilan",
  "number": "2839403725",
  "sortCode": "23-90-53",
  "active": true,
  "balance": 1657.59
}
```
